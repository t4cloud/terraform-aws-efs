locals {
  id = "${replace(var.name, " ", "-")}"
}

resource "aws_security_group" "efs" {
  name        = "${local.id}-EFS"
  description = "Access to ports EFS (2049)"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = ["${var.access_sg_ids}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags, map("Name", "${var.name} EFS"))}"
}

resource "aws_efs_file_system" "efs" {
  encrypted        = "${var.encrypted}"
  performance_mode = "${var.performance_mode}"
  creation_token   = "${var.creation_token}"
  tags             = "${merge(var.tags, map("Name", var.name))}"
}

resource "aws_efs_mount_target" "efs_mount" {
  count           = "${length(var.subnets)}"
  file_system_id  = "${aws_efs_file_system.efs.id}"
  subnet_id       = "${element(var.subnets, count.index)}"
  security_groups = ["${aws_security_group.efs.id}"]
}
